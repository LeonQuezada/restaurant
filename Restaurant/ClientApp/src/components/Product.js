import React, { useState,useEffect, Fragment,useRef } from 'react';


export function Product() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [products, setItems] = useState([]);

    useEffect(() => {
        fetch("api/v1/Product")
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setItems(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <Fragment>
                <NewProduct products={products} setItems={setItems} />
                <table className='table table-striped' aria-labelledby="tabelLabel">
                    <thead>
                        <tr>
                        <th>Name</th>
                        <th>SKU</th>
                        <th>quantity</th>
                        <th>Action</th>  
                        </tr>
                    </thead>
                    {products.map((product) => (
                        <TableProductItem key={product.sku} product={product} products={products} setItems={setItems}  />
                    ))}
                </table>
            </Fragment>
        );
    }
}

export function NewProduct({ setItems, products }) {

    const name_imput_ref = useRef();
    const sku_imput_ref = useRef();
    const quantity_imput_ref = useRef();

    const AddProject = () => {
        const newproducts = [...products];

        if (sku_imput_ref.current.value === '') return;
        const quantity = parseInt(quantity_imput_ref.current.value);

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ name: name_imput_ref.current.value, sku: sku_imput_ref.current.value, quantity: quantity })
        };

        fetch("api/v1/Product", requestOptions)
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.id!=0) {
                        newproducts.push(result)
                        setItems(newproducts);    
                    }                                    
                },
                (error) => {
                    console.log(error);
                }
            )

        return;
    }

    return (
        <Fragment>
            <label>Enter product:&nbsp;</label>
            <input ref={name_imput_ref} name="NameProduct" type="text" placeholder="Name" />
            <label>&nbsp;</label>
            <input ref={sku_imput_ref} type="text" placeholder="SKU" />
            <label>&nbsp;</label>
            <input ref={quantity_imput_ref} type="number" placeholder="Quantity" />
            <label>&nbsp;</label>
            <button className="btn btn-primary" onClick={AddProject}>+</button>
        </Fragment>
    );
}

export function TableProductItem({ product, products, setItems }) {
    const DeleteProduct = () => {
        const delProducts = [...products];
        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
        };
        fetch("api/v1/Product/" + product.id, requestOptions)
            .then(res => res.json())
            .then(
                (result) => {
                    var products = delProducts.filter(function (obj) {
                        return obj["id"] !== product.id
                    })
                    setItems(products);
                },
                (error) => {

                }
            )
    };
    return (
        <Fragment>
            <tbody>
                <tr>
                    <td>{product.name}</td>
                    <td>{product.sku}</td>
                    <td>{product.quantity}</td>
                    <td><button onClick={DeleteProduct} >Delete &#128465;</button></td>   
                </tr>
            </tbody>
        </Fragment>
    )
}


