﻿import React, { useState, useEffect, Fragment, useRef } from 'react';

export function Order() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [products, setItems] = useState([]);
    const orderItem = {}

    useEffect(() => {
        fetch("api/v1/Product")
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setItems(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <Fragment>
                <SaveOrder orderItem={orderItem} setItems={setItems} products={products} />
                <table className='table table-striped' aria-labelledby="tabelLabel">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>SKU</th>
                            <th>quantity</th>
                            <th>Action</th>                            
                        </tr>
                    </thead>
                    {products.map((product) => (
                        <TableProductItem key={product.sku} product={product} orderItem={orderItem} />
                    ))}
                </table>
            </Fragment>
        );
    }
}
export function SaveOrder({ orderItem, setItems, products }) {    
    const styles_save = {
        float: 'right',
        width: '300px',
        padding: '10px',
        margin: "20px"
    };

    const SaveData = (orderItem, setItems, products) => {
        const update_products = [...products];
        const productSend = []
        for (const [key, value] of Object.entries(orderItem)) {

            var ProductId = parseInt(key);
            var Quantity = parseInt(value['value'].value);

            var item = update_products.find((product) => { return product.id == key })


            if (item.quantity <= Quantity) {
                console.log("error ")
            } else {
                productSend.push({ "Product": ProductId, "Quantity": Quantity })

                item.quantity = item.quantity - Quantity;
                value['value'].value = 0;
                setItems(update_products)
            }
            
        }

        console.log({ products: productSend, statuOrder: 0 });
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({products: productSend, statuOrder: 0 })
        };
        fetch("api/v1/Order", requestOptions)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result); 
                },
                (error) => {
                    console.log(error);
                }
            )

    }
    return (<Fragment><button style={styles_save} onClick={() => { SaveData(orderItem, setItems, products)} } className="btn btn-primary" >Save</button></Fragment>);
}
export function TableProductItem({ product, orderItem }) {

    const add = (e) => {
         //console.log(orderItem, product, e.target.value)
        //console.log(e.target)
        orderItem[product.id] = { value: e.target }
    };
    return (
        <Fragment>
            <tbody>
                <tr>
                    <td>{product.name}</td>
                    <td>{product.sku}</td>
                    <td>{product.quantity}</td>
                    <td><input onChange={(e) => { add(e)}} type="number" placeholder="Quantity" /></td>
                </tr>
            </tbody>
        </Fragment>
    )
}