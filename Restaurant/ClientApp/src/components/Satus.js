﻿import React, { useState, useEffect, Fragment, useRef } from 'react';

export function Satus() {

    const styles_container = {
        border: '2px solid rgba(0, 0, 0, 0.15)',
    };

    const Select = (id) => {

        var element_tab = document.getElementById("myTabContent");
        var element_tab_menu = document.getElementById("myTab");

        const tab_array = Array.from(element_tab.children);
        const tab_menu_array = Array.from(element_tab_menu.children);



        var tab = 0;
        tab_menu_array.forEach(item => {
            tab++;
            if (tab == id) {
                item.firstChild.classList.add('active');
            } else {
                item.firstChild.classList.remove('active');
            }

        });


        tab = 0;
        tab_array.forEach(item => {
            tab++;
            if (tab == id) {
                item.classList.add('show', 'active');
            } else {
                item.classList.remove('show');
                item.classList.remove('active');
            }

        });
    };
    return (
        <Fragment>
            <div>
                <ul className="nav nav-tabs" id="myTab" role="tablist">
                    <li className="nav-item" role="presentation">
                        <button onClick={() => { Select(1) }} className="nav-link active" id="home-tab" type="button">Pending</button>
                    </li>
                    <li className="nav-item" role="presentation">
                        <button onClick={() => { Select(2) }} className="nav-link" id="profile-tab" type="button">In Process</button>
                    </li>
                    <li className="nav-item" role="presentation">
                        <button onClick={() => { Select(3) }} className="nav-link" id="contact-tab" type="button">Completed</button>
                    </li>
                    <li className="nav-item" role="presentation">
                        <button onClick={() => { Select(4) }} className="nav-link" id="contact-tab" type="button">Delivered</button>
                    </li>
                    <li className="nav-item" role="presentation">
                        <button onClick={() => { Select(5) }} className="nav-link" id="contact-tab" type="button">Canceled</button>
                    </li>
                </ul>
                <div className="tab-content" id="myTabContent">
                    <div className="tab-pane fade show active" id="pending" role="tabpanel">
                        <div className="container" style={styles_container}>
                            <AddCard statuOrder={0} />
                        </div>
                    </div>
                    <div className="tab-pane fade" id="in-process" role="tabpanel">
                        <div className="container" style={styles_container}>
                            <AddCard statuOrder={1} />
                        </div>
                    </div>
                    <div className="tab-pane fade" id="completed" role="tabpanel">
                        <div className="container" style={styles_container}>
                            <AddCard statuOrder={2} />
                        </div>
                    </div>
                    <div className="tab-pane fade" id="in-process" role="tabpanel">
                        <div className="container" style={styles_container}>
                            <AddCard statuOrder={3} />
                        </div>
                    </div>
                    <div className="tab-pane fade" id="completed" role="tabpanel">
                        <div className="container" style={styles_container}>
                            <AddCard statuOrder={4} />
                        </div>
                    </div>
                </div>
            </div>
        </Fragment >
    );
}

export function AddCard({ statuOrder }) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [order, setItems] = useState([]);    
    
    useEffect(() => {        
        fetch("api/v1/Order/")
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setItems(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])
    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <Fragment>
                <div className="row row-cols-1">
                    <div className="col-5">

                        {order.map(item =>                            
                            <div key={item.id}  className="card">
                                <div className="card-body">
                                    <CardItem order={item.products} />
                                </div>
                            </div>
                        )}                        
                    </div>
                </div>
            </Fragment>
        );
    }
}

export function CardItem({ order }) {
    return (
        <Fragment>
            {order.map(item =>
                <div key={item.product} >
                    <label>{item.quantity}</label>
                    <br></br>
                    <label>{item.product}</label>
                </div>
            )}             
        </Fragment>
        );
}