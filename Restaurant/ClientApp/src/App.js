import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Satus } from './components/Satus';
import { Product } from './components/Product';
import { Order } from './components/Order';


import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Satus} />
        <Route path='/Order' component={Order} />
        <Route path='/Product' component={Product} />
      </Layout>
    );
  }
}
