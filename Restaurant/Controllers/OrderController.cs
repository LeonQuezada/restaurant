﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Restaurant.Data;
using Restaurant.Model;

namespace Restaurant.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrderController : Controller
    {
        private readonly IMemory _memory;
        public OrderController(IMemory memory)
        {
            _memory = memory;
        }

        // POST: api/v1/Product
        [HttpPost]
        public IActionResult Create(Order Order)
        {
            try
            {
                return Ok(_memory.NewOrder(Order));
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/v1/Product
        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                return Ok(_memory.GetOrders());
            }
            catch
            {
                return BadRequest();
            }
        }

        // GET: api/v1/Product/{int}
        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            try
            {
                return Ok(_memory.GetForStatuOrder((StatuOrder)id));
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
