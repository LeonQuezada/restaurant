﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Restaurant.Data;
using Restaurant.Model;
using System.Threading.Tasks;

namespace Restaurant.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]    
    public class ProductController : Controller
    {
        private readonly IMemory _memory;
        public ProductController(IMemory memory)
        {
            _memory=memory;
        }

        // POST: api/v1/Product
        [HttpPost]
        public IActionResult Create(Product product)
        {
            try
            {
                return Ok(_memory.NewProduct(product));
            }
            catch
            {
                return BadRequest();
            }
        }


        // GET: api/v1/Product
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_memory.GetProdcuts());
            }
            catch
            {
                return BadRequest();
            }
        }

        // DELETE: api/v1/Product
        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            try
            {
                return Ok(_memory.DeleteProdcut(id));
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
