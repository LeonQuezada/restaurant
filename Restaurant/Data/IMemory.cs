﻿using Restaurant.Model;
using System.Collections.Generic;

namespace Restaurant.Data
{
    public interface IMemory
    {
        Order NewOrder(Order order);
        List<Order> GetOrders();
        List<Order> GetForStatuOrder(StatuOrder id);

        Product NewProduct(Product product);
        List<Product> GetProdcuts();
        ResponseMemory DeleteProdcut(int id);
    }
}
