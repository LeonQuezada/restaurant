﻿namespace Restaurant.Data
{
    public class ToolsMemory
    {
        private static int _orderAutoincrement { get; set; } = 0;
        private static int _productAutoincrement { get; set; } = 0;

        public static int OrderAutoincrement()
        {
            _orderAutoincrement++;
            return _orderAutoincrement;
        }


        public static int ProductAutoincrement()
        {
            _productAutoincrement++;
            return _productAutoincrement;
        }
    }
}
