﻿using Restaurant.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Restaurant.Data
{
    public class Memory : IMemory
    {
        private List<Order> _orders;
        private List<Product> _products;

        private object _lockProduct = new object();
        private object _lockOrderStock = new object();

        public Memory()
        {
            _orders = new List<Order>();
            _products = new List<Product>();
        }
        
        public List<Order> GetForStatuOrder(StatuOrder id)
        {
            List<Order>  order = _orders.Where(o => o.StatuOrder == id).ToList();
            if (order==null)
            {
                return null;
            }
            else
            {
                return order;
            }
        }
        public List<Order> GetOrders()
        {
            return _orders;
        }
        public Order NewOrder(Order order)
        {
            if (order?.Products?.Count > 0)
            {
                lock (_lockProduct)
                {
                    bool delete = false;
                    foreach (var product in order.Products)
                    {
                        var product1 = _products.FirstOrDefault(p => p.Id == product.Product);
                        if (product1?.Quantity>= product.Quantity)
                        {
                            Console.WriteLine(product1);
                        }
                        else
                        {
                            delete = true;
                        }                        
                    }
                    if (delete)
                    {
                        return order;
                    }
                    else
                    {
                        foreach (var product in order.Products)
                        {
                            var product1 = _products.FirstOrDefault(p => p.Id == product.Product);
                            product1.Quantity = product1.Quantity - product.Quantity;
                        }
                        lock (_lockOrderStock)
                        {
                            order.Id = ToolsMemory.OrderAutoincrement();
                            _orders.Add(order);
                        }
                        return order;
                    }
                }                                                            
            }
            else
            {
                return order;
            }
            
        }



        #region order
        public Product NewProduct(Product product)
        {
            if (product?.SKU==null)
            {
                return product;
            }
            Product Exists = null;
            product.Id = 0;
            lock (_lockProduct)
            {
                Exists = _products.FirstOrDefault(p => p.SKU == product.SKU);
            }            
            if (Exists != null)
            {
                return product;
            }
            else
            {
                product.Id = ToolsMemory.ProductAutoincrement();
                _products.Add(product);
                return product;
            }
        }

        public ResponseMemory DeleteProdcut(int id) {
            var productDel = _products.FirstOrDefault(p => p.Id == id);
            if (productDel!=null)
            {
                _products.Remove(productDel);
                return ResponseMemory.ok;
            }
            else
            {
                return ResponseMemory.error;
            }            
        }

        public List<Product> GetProdcuts()
        {
            return _products;
        }

        #endregion
    }
}
