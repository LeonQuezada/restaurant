﻿using System.Collections.Generic;

namespace Restaurant.Model
{
    public class Order
    {
        public int Id { get; set; }
        public List<ItemOrder> Products { get; set; }
        public StatuOrder StatuOrder { get; set; }
    }

    public class ItemOrder
    {
        public int Product { get; set; }
        public int Quantity { get; set; }
    }

    public enum StatuOrder
    {
        Pending,
        InProcess,
        Completed,
        Delivered,
        Canceled
    }
}
